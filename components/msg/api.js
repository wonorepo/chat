const Router = require('koa-router')
const uuidv4 = require('uuid/v4')

let api = new Router({ prefix: '/messages' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.chat_app.models
    return next()
})

api.get('/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        const docid = ctx.state.models.msg.getDocId(id)
        let msg = await ctx.state.models.msg.getById(docid)

        ctx.status = 200
        ctx.body = msg

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404)
        }
        throw err
    }

})

module.exports = api