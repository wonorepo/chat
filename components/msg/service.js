const uuidv4 = require('uuid/v4')

module.exports = class {

    constructor(model) {
        this.msgModel = model
    }

    async create(data, chatId, ethAddress){
        
        data.owner = ethAddress
        data.timestamp_created = Date.now()
        data.chatId = chatId

        const id = uuidv4()
        const docid = this.msgModel.getDocId(id)

        return this.msgModel.create(docid, data, id)

    }

    async update(id, data){
        
        data.timestamp_updated = Date.now()
        const docid = this.msgModel.getDocId(id)
        
        return this.msgModel.update(docid, data)

    }

    async get(id){

        const docid = this.msgModel.getDocId(id)

        return this.msgModel.getById(docid)

    }

}