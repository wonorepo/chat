const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('send_msg', async (data, fn) => {

        try {

            logger.info(`[send_msg] ${JSON.stringify(data, null, 2)}`)

            const message = await socket.services.msg.create(data.msg, data.chatId, socket.ethAddress)

            io.to(message.chatId).emit('new_msg', message)

            io.of('/').in(message.chatId).clients((err, clients) => {

                if (err){
                    return fn({
                        error: true,
                        errorMessage: err.message
                    })
                } 

                let recivers = clients.filter(item => item != socket.id && item.split('::')[0] != socket.ethAddress)

                for (let r of recivers) {

                    io.sockets.sockets[r].emit('notification', {
                        type: 'msg',
                        data: message
                    });

                }

            })

            fn(message)

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            throw err
        }

    })

}