const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('read_msg', async (data) => {
        
        try {

            logger.info(`[read_msg] ${JSON.stringify(data, null, 2)}`)

            const message = await socket.services.msg.update(data.id, { readed: true })
            
            io.to(message.chatId).emit('msg_readed', message)

        } catch (err) {
            logger.error(err.stack)
        }

    })

}