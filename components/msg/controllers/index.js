const send_msg = require('./send_msg')
const read_msg = require('./read_msg')

module.exports = {
    send_msg,
    read_msg
}