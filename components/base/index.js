const Model = require('./model')
const Dictionary = require('./dictionary')

module.exports = { Model, Dictionary }