const _ = require('lodash')
const Ajv = require('ajv')
const logger = require('../../services/logger')

module.exports = class Model {

    constructor({ model, bucket, cb }) {
        this._name = model.name
        this._counterName = model.counter ? model.counter.name : ''
        this._schema = model.schema || {}
        this._bucket = bucket
        this._bucketName = bucket._name
        this._cb = cb
        this._n1ql = cb.N1qlQuery;
        this.SearchQuery = cb.SearchQuery
        this._ajv = new Ajv()
    }

    get name() {
        return this._name
    }

    get schema() {
        return this._schema
    }

    get bucketName() {
        return this._bucketName
    }

    validate(doc) {
        let valid = this._ajv.validate(this._schema, doc)
        return {
            valid: valid,
            errors: this._ajv.errors
        }
    }

    getDocId(id) {
        return `${this._name}::${id}`
    }

    async getId() {
        return (await this._bucket.counterAsync(this._counterName, 1)).value
    }

    async create(docid, doc, id) {
        id ? doc.id = id : doc.id = await this.getId()
        doc.type = this._name
        await this._bucket.insertAsync(docid, doc)
        return doc
    }

    async getById(docid) {
        let doc = await this._bucket.getAsync(docid)
        return doc.value
    }

    async getMulti(docids) {

        let values = []

        if (docids.length != 0) {
            let res = await await this._bucket.getMultiAsync(docids)
            for (let docid in res) {
                values.push(res[docid].value)
            }
        }

        return values

    }

    async update(docid, doc) {
        let olddoc = await this._bucket.getAsync(docid)
        let newdoc = _.merge(olddoc.value, doc)
        await this._bucket.replaceAsync(docid, newdoc, { cas: olddoc.cas })
        return newdoc
    }

    async remove(docid) {
        await this._bucket.removeAsync(docid)
        return docid
    }

    async find({ condition, attributes = ['*'], sort=[], limit, offset}) {

        let sortParser = (sortArr) => {
            let res = ''
            let delimeter = ','
            sortArr.map(item => {
                if (item[0] === '-') {
                    res += `${item.slice(1)} DESC${delimeter}`
                } else {
                    res += `${item} ASC${delimeter}`
                }
            })
            return res.slice(0, -1)
        }

        let whereClause = `type = "${this._name}"${condition ? ' AND (' + condition + ')' : ''}`
        let seletClause = attributes.reduce((a, b) => a + ',' + b)
        let sortClause = sort.length != 0 ? `ORDER BY ${sortParser(sort)}` : 'ORDER BY id'
        let limitCluase = limit ? `LIMIT ${limit}` : ''
        let offsetCluase = offset ? `OFFSET ${offset}` : ''

        seletClause = seletClause === '*' ? `${this._bucketName}.*` : seletClause

        const queryString = `SELECT ${seletClause} FROM ${this._bucketName} WHERE ${whereClause} ${sortClause} ${limitCluase} ${offsetCluase}`

        logger.info(queryString)

        let query = this._n1ql.fromString(queryString);

        return await this._bucket.queryAsync(query)

    }

    async findOne({ condition, attributes = ['*'] }) {
        let res = await this.find({ condition, attributes, limit: 1, offset: 0 })
        return res.length != 0 ? res[0] : null
    }

    async search(query) {
        return await this._bucket.queryAsync(query)
    }

    async count({condition=''}){
        
        let whereClause = `type = "${this._name}"${condition ? ' AND (' + condition + ')' : ''}`
        let seletClause = `count(*) as size`

        const queryString = `SELECT ${seletClause} FROM ${this._bucketName} WHERE ${whereClause} GROUP BY type`

        logger.info(queryString)

        let query = this._n1ql.fromString(queryString)

        let res = await this._bucket.queryAsync(query)

        return res.length != 0 ? res[0].size : 0

    }

}