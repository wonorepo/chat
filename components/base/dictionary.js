
module.exports = class Dictionary {

    constructor(name, bucket){
        this._name = name
        this._bucket = bucket
    }

    get name(){
        return this._name
    }

    async getValue(key){
        return await this._bucket.mapGetAsync(this._name, key)
    }

    async getMap(){
        return (await this._bucket.getAsync(this._name)).value
    }

}