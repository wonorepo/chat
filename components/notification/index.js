const model = require('./service')
const listeners = require('./controllers')

module.exports = { model, listeners }