const conf = require('config')
const axios = require('axios')

class Notification {

    constructor() {
        this._api = axios.create({
            baseURL: conf.get('profileService.base_url') + '/notifications',
            headers: { 'X-Signature': `api-key ${conf.get('profileService.api_key')}`}
        })
    }

    async create(body, ethAddress){
        return await this._api.post(`/`, body, {
            headers: {
                'X-Eth-Address': ethAddress
            }
        })
    }

    async update(id, body, ethAddress){
        return await this._api.put(`/${id}`, body, {
            headers: {
                'X-Eth-Address': ethAddress
            }
        })
    }

    async get(id, ethAddress){
        return await this._api.get(`/${id}`, {
            headers: {
                'X-Eth-Address': ethAddress
            }
        })
    }

    async delete(id, ethAddress){
        return await this._api.delete(`/${id}`, {
            headers: {
                'X-Eth-Address': ethAddress
            }
        })
    }

}

module.exports = new Notification()