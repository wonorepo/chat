const model = require('../service')
const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('read_notification', async (data, fn) => {

        try {

            logger.info(`[read_notification] ${JSON.stringify(data, null, 2)}`)

            io.of('/').clients(async (err, clients) => {

                if (err) {
                    fn({
                        error: true,
                        errorMessage: err.message
                    })
                    throw err
                }

                let recivers = clients.filter(item => item === socket.id)

                fn(recivers.lenght)

                await model.delete(data.id, socket.ethAddress)

                for (let r of recivers) {
                    io.sockets.sockets[r].emit('deleted_notification');
                }

            })

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            throw err
        }

    })

}