const model = require('../service')
const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('new_notification', async (data, fn) => {
        
        try {

            logger.info(`[new_notification] ${JSON.stringify(data, null, 2)}`)

            const notification = (await model.create(data, data.to)).data
            
            io.of('/').clients((err, clients) => {

                if (err){
                    fn({
                        error: true,
                        errorMessage: err.message
                    })
                    throw err
                } 

                let recivers = clients.filter(item => item.split('::')[0] === data.to)

                fn(recivers.lenght)

                for (let r of recivers) {
                    io.sockets.sockets[r].emit('notification', data);
                }

            })

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            logger.error(err.stack)
        }

    })

}