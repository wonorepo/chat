const read_notification = require('./read_notification')
const new_notification = require('./new_notification')

module.exports = {
    read_notification,
    new_notification
}