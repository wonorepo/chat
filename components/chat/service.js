const uuidv4 = require('uuid/v4')

module.exports = class {

    constructor(model) {
        this.chatModel = model
    }

    async create(data, ethAddress) {
        data.owner = ethAddress
        const id = uuidv4()
        const docid = this.chatModel.getDocId(id)
        return this.chatModel.create(docid, data, id)
    }

    async update(id, data) {
        const docid = this.chatModel.getDocId(id)
        return this.chatModel.update(docid, data)
    }

    async get(id) {
        const docid = this.chatModel.getDocId(id)
        return this.chatModel.getById(docid)
    }

    async getTotalChats(ethAddress) {

        let condition =
            `'${ethAddress.toLowerCase()}' IN members`

        return this.chatModel.count(
            {
                condition: condition
            }
        )

    }

    async getChats({ ethAddress, fields = 'id', limit = 10, offset = 0 }) {

        let condition =
            `'${ethAddress.toLowerCase()}' IN members`
    
        return this.chatModel.find(
            {
                condition: condition,
                attributes: fields.split(','),
                limit: limit,
                offset: offset
            }
        )

    }

}