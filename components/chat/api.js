const Router = require('koa-router')

let api = new Router({ prefix: '/chats' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.chat_app.models
    return next()
})

api.get('/', async (ctx, next) => {

    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    const total = await ctx.state.models.chat.count(
        {
            condition: filter
        }
    )

    let chats = await ctx.state.models.chat.find(
        {
            condition: filter,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: chats.length,
        total: total,
        chats: chats
    }

})
api.get('/:id/messages', async (ctx, next) => {

    const { id } = ctx.params
    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let condition =
        `chatId = "${id}"` +
        `${filter ? ` AND ${filter}` : ''}`

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : ['-timestamp_created']

    const total = await ctx.state.models.msg.count(
        {
            condition: condition
        }
    )

    let msgs = await ctx.state.models.msg.find(
        {
            condition: condition,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: msgs.length,
        total: total,
        messages: msgs
    }

})
api.get('/:id/messages/count', async (ctx, next) => {

    const { id } = ctx.params;
    const profileId = ctx.query.profileId

    if (!profileId) ctx.throw(400, "Provide profileId query parameter")

    const count = await ctx.state.models.msg.count(
        {
            condition: `chatId = '${id}' AND owner != '${profileId.toLowerCase()}' AND readed = false`
        }
    )

    ctx.status = 200
    ctx.body = {
        count
    }

})
api.get('/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        const docid = ctx.state.models.chat.getDocId(id)
        let chat = await ctx.state.models.chat.getById(docid)

        ctx.status = 200
        ctx.body = chat

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404)
        }
        throw err
    }

})

module.exports = api