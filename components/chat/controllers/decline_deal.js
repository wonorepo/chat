const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('decline_deal', async (data, fn) => {
        
        try {

            logger.info(`[decline_deal] ${JSON.stringify(data, null, 2)}`)

            let chat = await socket.services.chat.get(data.chatId)
            
            if(chat.deal){
                chat.deal.declined = true
                await socket.services.chat.update(data.chatId, chat)
            }
                        
            fn(chat)

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            logger.error(err.stack)
        }

    })

}