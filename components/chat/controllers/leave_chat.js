const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('leave_chat', async (data) => {
        
        try {

            logger.info(`[leave_chat] ${JSON.stringify(data, null, 2)}`)
            
            socket.leave(data.chatId)

            //fn(data.chatId)

        } catch (err) {
            // fn({
            //     error: true,
            //     errorMessage: err.message
            // })
            logger.error(err.stack)
        }

    })

}