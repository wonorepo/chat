const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('join_chat', async (data) => {
        
        try {

            logger.info(`[join_chat] ${JSON.stringify(data, null, 2)}`)

            let chat = await socket.services.chat.get(data.chatId)
            
            if(!chat.members.includes(socket.ethAddress)){
                chat.members.push(socket.ethAddress)
                await socket.services.chat.update(chat.id, chat)
            }
            
            socket.join(chat.id, ()=>{
                socket.emit('member_joined', chat)
            })
            
        } catch (err) {
            logger.error(err.stack)
        }

    })

}