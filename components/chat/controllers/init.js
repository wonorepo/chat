const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('init', async (fn) => {

        try {
            
            logger.info(`[init] Socket - ${socket.id}`)

            let totalChats = await socket.services.chat.getTotalChats(socket.ethAddress)

            if(totalChats != 0){
                let chats = await socket.services.chat.getChats({ethAddress: socket.ethAddress, limit: totalChats})
                socket.join(chats.map(item=>item.id), (err)=>{
                    if(err){
                        return fn({
                            error: true,
                            errorMessage: err.message
                        })
                    } 
                    fn(totalChats)
                })
            }

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            logger.error(err.stack)
        }

    })

}
