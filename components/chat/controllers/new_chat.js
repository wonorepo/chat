const logger = require('../../../services/logger')

module.exports = (socket, io) => {

    socket.on('new_chat', async (data, fn) => {

        try {

            logger.info(`[new_chat] ${JSON.stringify(data, null, 2)}`)

            let messages = data.messages ? data.messages : []
            delete data.messages

            let totalChats = await socket.services.chat.getTotalChats(socket.ethAddress)
            let chats = await socket.services.chat.getChats({ ethAddress: socket.ethAddress, fields: 'id,asset', limit: totalChats })

            for (let chat of chats) {
                if (data.asset == chat.asset) {
                    logger.info(`Chat already exist ${JSON.stringify(data, null, 2)}`)
                    fn({
                        error: true,
                        errorMessage: "Chat already exist"
                    })
                    return
                }
            }

            let chat = await socket.services.chat.create(data, socket.ethAddress)

            for (let msg of messages) {
                let res = await socket.services.msg.create(msg, chat.id, socket.ethAddress)
                logger.info(`[msg_created] ${JSON.stringify(res, null, 2)}`)
            }

            socket.join(chat.id, (err) => {
                if (err) {
                    return fn({
                        error: true,
                        errorMessage: err.message
                    })
                }
            })

            socket.emit('chat_created', chat)
            
            fn(chat)

        } catch (err) {
            fn({
                error: true,
                errorMessage: err.message
            })
            logger.error(err.stack)
        }

    })

}
