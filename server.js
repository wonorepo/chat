const db = require('./db/couchbaseDB')
const logger = require('./services/logger')
const config = require('config')
const appRestApiFabric = require('./appRestApi')
const appSocketApiFabric = require('./appSocketApi')

const chat = require('./components/chat')
const msg = require('./components/msg')
const notification = require('./components/notification')

logger.info(`DB sync...`)
db.sync()
.then(()=>{

    logger.info(`DB synchronized`)
    
    services = {
        chat: new chat.Service(db.buckets.chat_app.models.chat),
        msg: new msg.Service(db.buckets.chat_app.models.msg)
    }

    const appRestApi = appRestApiFabric(db)
    const appSocketApi = appSocketApiFabric(services)

    const server = require('http').createServer(appRestApi.callback())
    
    appSocketApi.attach(server)

    server.listen(config.get('port'))
    
    logger.info(`Server is running on port ${config.get('port')}`)

})
.catch(err=>{
    logger.error(err, err.stack)
    process.exit(1)
})