const winston = require('winston')
const { combine, timestamp, label, printf, json, colorize } = winston.format

const myFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});

const logger = winston.createLogger({
    level: 'info',
    format: combine(
        json(),
        colorize()
    ),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.Console({
            format: combine(
                timestamp(),
                colorize(),
                myFormat
            ),
            level: 'error'
        })
    ]
});

if (process.env.NODE_ENV !== 'prod') {
    logger.add(new winston.transports.Console({
        format: combine(
            timestamp(),
            colorize(),
            myFormat
        ),
        level: 'debug'
    }));
}

module.exports = logger