ARG NODE_VERSION=10.11
FROM node:${NODE_VERSION}

COPY package.json yarn.lock /api/
RUN cd /api && yarn install --production

COPY . /api
WORKDIR /api

ARG ENV=dev
ENV NODE_ENV=${ENV}

EXPOSE 3000

CMD ["yarn", "start"]