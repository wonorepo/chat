const SocketIO = require('socket.io')
const logger = require('./services/logger')

const chat = require('./components/chat')
const msg = require('./components/msg')
const notification = require('./components/notification')

module.exports = (services) => {
    
    const io = new SocketIO()

    io.use((socket, next) => {
        // Authentication
        return next()
    })
    
    io.use((socket, next) => {
        let ethAddress = socket.handshake.query.eth_address

        if (!ethAddress) return next(new Error('Provide eth_address params'))
        
        if (!ethAddress.match(/^0x[a-fA-F0-9]{40}$/)) return next(new Error('Invalid eth_address'))
        
        socket.services = services
        socket.ethAddress = ethAddress.toLowerCase()
        socket.id = `${socket.ethAddress}::${socket.id}`
        return next()

    })
    
    io.on('connection', function (socket) {
    
        logger.info(`Socket - ${socket.id} connected`)
    
        socket.on('error', (err) => {
            logger.error(err.stack, err)
        })
    
        //register listeners for chat
        for (let listener in chat.listeners) {
            chat.listeners[listener](socket, io)
        }
    
        //register listeners for msg
        for (let listener in msg.listeners) {
            msg.listeners[listener](socket, io)
        }
    
        //register listeners for notification
        for (let listener in notification.listeners) {
            notification.listeners[listener](socket, io)
        }
    
    })

    return io

}