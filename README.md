# Emit cheatsheet #
    
    // create chat
    socket.emit('new_chat', data, cb)

    // join to chat
    socket.emit('join_chat', data)

    // join to all chat rooms
    socket.emit('init', cb)

    // leave chat
    socket.emit('leave_chat', data)

    // decline deal
    socket.emit('decline_deal', data, cb)

    //send message
    socket.emit('send_msg', data, cb)

    //read message
    socket.emit('read_msg', data)