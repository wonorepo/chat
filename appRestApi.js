const Koa = require('koa')
const Router = require('koa-router');
const logger = require('./services/logger')
const koalogger = require('koa-logger')
const cors = require('@koa/cors')

const chat = require('./components/chat')
const msg = require('./components/msg')
const notification = require('./components/notification')

module.exports = (db) => {
	
	const app = new Koa()
	const router = new Router({ prefix: '/api' })

	router.use(chat.api.routes())
	router.use(msg.api.routes())
	
	app.on('error', (err) => {
		logger.error(err.stack)
	})
	
	app
		.use(cors())
		.use((ctx, next) => {
			ctx.set('Access-Control-Allow-Origin', '*')
			return next()
		})
		.use((ctx, next)=>{
			ctx.db = db
			ctx.buckets = db.buckets
			return next()
		})
		.use(koalogger())
		.use(router.routes())
	
	return app

}

