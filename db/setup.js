
module.exports = Object.freeze({
    buckets: [
        {
            name: 'chat_app',
            opts: {
                ramQuotaMB: 100
            },
            models: [
                {
                    name: 'chat'
                },
                {
                    name: 'msg'
                }
            ],
            indexes: [
                {
                    attribute: 'type'
                },
                {
                    attribute: 'chatId'
                }
            ]
        }
    ]
})